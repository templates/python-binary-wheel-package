# {{cookiecutter.project_name}}

![Build status]({{cookiecutter.project_url}}/badges/main/pipeline.svg)
![Test coverage]({{cookiecutter.project_url}}/badges/main/coverage.svg)
![Latest release]({{cookiecutter.project_url}}/badges/main/release.svg)

{{cookiecutter.description}}

## Installation
```
pip install .
```

## Usage
```python
from {{cookiecutter.project_slug}} import cool_module

cool_module.greeter()   # prints "Hello World"
```

## Contributing

To contribute, please create a feature branch and a "Draft" merge request.
Upon completion, the merge request should be marked as ready and a reviewer
should be assigned.

Verify your changes locally and be sure to add tests. Verifying local
changes is done through `tox`.

```pip install tox```

With tox the same jobs as run on the CI/CD pipeline can be ran. These
include unit tests and linting.

```tox```

To automatically apply most suggested linting changes execute:

```tox -e format```

## License
This project is licensed under the Apache License Version 2.0
