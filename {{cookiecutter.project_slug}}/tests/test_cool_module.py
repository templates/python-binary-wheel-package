"""Testing of the Cool Module"""
from unittest import TestCase

from {{cookiecutter.project_slug}}.cool_module import greeter


class TestCoolModule(TestCase):
    """Test Case of the Cool Module"""

    def test_greeter(self):
        """Testing that the greeter does not crash"""
        greeter()
        self.assertEqual(2 + 2, 4)
