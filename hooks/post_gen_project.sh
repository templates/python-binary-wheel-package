#!/bin/bash

git init
git submodule add https://github.com/LecrisUT/CMakeExtraUtils.git cmake/cmake-extra-utils
git add --all
git commit -m "initial commit" --no-edit
git tag v0.0.1