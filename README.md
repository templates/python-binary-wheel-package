# Example Python Binary Extension Wheel Package

![Build status](https://git.astron.nl/templates/python-binary-wheel-package/badges/main/pipeline.svg)
![Test coverage](https://git.astron.nl/templates/python-binary-wheel-package/badges/main/coverage.svg)

An example repository of an CI/CD pipeline for building, testing and publishing
a python package  that uses binary extensions compiled as wheels.

If you find some missing functionality with regards to CI/CD, testing, linting
or something else, feel free to make a merge request with the proposed changes.

### Features:

- Compile C / C++11 and bind with Python
  - Easily define any C / C++ dependency to be included using `before-all = "apt / yum xxx"`
  https://cibuildwheel.readthedocs.io/en/stable/options/#examples
- Automatic versioning using semantic version tags (`git tag vX.X.X`)
  - Versions from `setuptools-scm` in Python are bound and forwarded to the
  C / C++ extension using the `DynamicVersion` git submodule.
- Run any task performed by CI/CD locally such as:
  - linting (`tox -e black`, `tox -e pylint`, `tox -e pep8`)
    - automatically format code (`tox -e format`)
  - unit tests (`tox -e py37`)
  - code coverage (`tox -e coverage`)
  - building & packaging (`tox -e build-local`, `tox -e build-ci-linux`)
- Automatically publish new releases to the gitlab package repository
- CI/CD uses [docker base image](docker/ci-runner/Dockerfile) to speed up pipeline

## How to apply this template

This templates uses `cookiecutter` which can be installed easily:

```bash
pip install --user cookiecutter
```

Then you can create an instance of your template with:

```bash
cookiecutter https://git.astron.nl/templates/python-binary-wheel-package.git
# Next follow a set of prompts (such as the name and description of the package)
```

## Gitlab security, secrets and role configuration

When using these templates for a repository on git.astron.nl please read the following
pages to configure Gitlab appropriately:

1. [Gitlab Repository Configuration](https://git.astron.nl/groups/templates/-/wikis/Gitlab-Repository-Configuration)
2. [Continuous delivery guideline](https://git.astron.nl/groups/templates/-/wikis/Continuous%20Delivery%20Guideline)

## License

This project is licensed under the Apache License Version 2.0